#include "TextureManager.h"

TextureManager::TextureManager(void)
{
	mNextID = 1;
}

TextureManager::~TextureManager(void)
{
	for (auto &v : mTextures)
	{
		v.second->Release();
		v.second = nullptr;
	}
}

int TextureManager::LoadTexture(_In_ ID3D11Device & d3dDevice, 
								 _In_z_ const wchar_t* szFileName)
{
	ID3D11ShaderResourceView * textureRV;

    DirectX::CreateDDSTextureFromFile(&d3dDevice, szFileName, nullptr, &textureRV);

	mTextures[mNextID] = textureRV;

	return mNextID++;
}
