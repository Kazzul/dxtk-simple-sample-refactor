#pragma once

#include "DDSTextureLoader.h"

#include <map>

using namespace std;

class TextureManager
{
private:
	map<int, ID3D11ShaderResourceView *> mTextures;

	int mNextID;

public:
	TextureManager(void);
	~TextureManager(void);

	int LoadTexture(_In_ ID3D11Device & d3dDevice, 
					 _In_z_ const wchar_t* szFileName);

	ID3D11ShaderResourceView & GetTexture(int id)
	{
		return *mTextures[id];
	}
};

