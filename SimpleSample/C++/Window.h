#pragma once

#include "resource.h"
#include <windows.h>

class Window
{
private:
	HWND hWnd;

public:
	Window(void);
	~Window(void);

	HWND getWindowHandle() const { return hWnd; };

	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
};

